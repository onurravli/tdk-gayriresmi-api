## TDK Gayriresmi API Hizmeti

<p>
Bu hizmet, TDK ya da benzeri kurumlarla ilişkili değildir, <a href="https://github.com/onurravli/">Onur Ravli</a> tarafından <a href="https://github.com/onurravli/tdk-gayriresmi-api">açık kaynaklı olarak</a> geliştirilmektedir. </p>

### Kullanım

<p>API hizmetini kullanabilmek için gerekli endpoint
<code>/api?madde=MADDE_ADI</code> şeklindedir.</p>
