const express = require('express');
const app = express();
const fs = require('fs');
const portNumber = process.env.PORT || 3000;

let data;

app.use(express.static('public'));

fs.readFile('./src/gts.json', (err, jsonString) => {
	if (err) {
		console.log('Dosya okunurken hata olustu: ', err);
		return;
	}
	try {
		data = JSON.parse(jsonString);
	} catch (err) {
		console.log('JSON dosyasi ayristirilirken hata olustu: ', err);
	}
});

app.get('/', (req, res) => {
	res.sendFile('index.html', { root: path.join(__dirname, 'public') });
});

app.get('/api', (req, res) => {
	const madde = req.query.madde;
	const anlam = data.find((d) => d.madde === madde);
	console.log(`${madde} maddesi icin sorgu yapiliyor.`);
	if (anlam) {
		console.log(`${madde} maddesi icin ${anlam.anlam} anlami bulundu.`);
		res.json(anlam);
	} else {
		res.json({ error: `${madde} maddesi bulunamadi.` });
	}
});

app.listen(portNumber, () => {
	console.log(`API servisi ${portNumber} portunda calisiyor.`);
});

module.exports = app;
